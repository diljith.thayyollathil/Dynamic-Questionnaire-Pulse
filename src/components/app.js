import React , {PropTypes} from 'react';

class app extends React.Component{
	render(){
		return(
			<div className="col-lg-12 no-sidepadder main-app-container">
			{this.props.children}
			</div>
			)
	}
}
app.proptypes={
	children:PropTypes.object.isRequired
}
export default app;