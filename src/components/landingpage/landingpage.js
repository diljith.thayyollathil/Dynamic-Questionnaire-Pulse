import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as landingactions from '../../actions/landingactions';

class landingpage extends React.Component{
	constructor(props ,state){
		super(props ,state);
		this.state={
			userdetails:{username:""},
			questions:{}
		}
		this.getallques=this.getallques.bind(this);
		this.returnform=this.returnform.bind(this);
		this.props.actions.getallquestion();
	}
	componentDidUpdate(nextProps) {
		// {this.props.questions.questions.map(answer=>{
		// 	console.log(answer);
		// 	{answer.type==='checkbox' && console.log("hello dude")}
		// 	{answer.type==='radio' && console.log("hello dudes")}
			
		// })}
	}
	returnform(questions){
		
	}
	getallques(){
		this.props.actions.getallquestion();
	}
	render(){  
		const {questions} = this.props;
		return(
			<div className="col-lg-12 no-sidepadder">
				<div className="col-lg-12 myapp-header ">
					<span className="hwader-user">Welcome {this.props.userdetails[0].username}</span>
				</div>
				<br />
				<button className="btn btn-info show-all" onClick={this.getallques}>SHOW ALL QUES</button>
				<div className="col-lg-12">
					{questions.questions.map(answer=>{
						return (<div key={answer._id} className='quesrow col-lg-12'>
							<span className="question">{answer.question}</span>
							{answer.type==='checkbox' && <div>{answer.answers.map(answerse =>{
								return(<div className="col-lg-6 row-adjuster">
									<input type="checkbox" name="vehicle" value={answerse} />{answerse}
								</div>)
							})}
							</div>
						}
						{answer.type==='radio' && <div>{answer.answers.map(answerse =>{
								return(<div className="col-lg-6 row-adjuster">
									<input type="radio" name="vehicle" value={answerse} />{answerse}
								</div>)
							})}
							</div>
						}
						{answer.type==='textarea' && 
								<div className="col-lg-12 textareacont">
									<textarea className="form-control"></textarea>
								</div>	
						}
						{answer.type==='textbox' && 
								<div className="col-lg-12 textareacont">
									<input type="text" className="form-control"></input>
								</div>	
						}
						{answer.type==='select' &&<select className="selectlist form-control">{answer.answers.map(answerse =>{
								return(<option>{answerse}</option>)
							})}
							</select>
						}
							</div>)				
					})}
				</div>
			</div>
			)
	}

}
landingpage.propTypes = {
  questions: PropTypes.array.isRequired
}


function mapStateToProps(state , ownProps){
	return {
		userdetails:state.logins,
		questions:state.questions
	};
}
function mapDispatchToProps(dispatch){
	return{
		actions :bindActionCreators(landingactions , dispatch)
	};
}
export default  connect(mapStateToProps , mapDispatchToProps)(landingpage);