import React from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as loginActions from '../../actions/loginactions';

class HomePage extends React.Component{
	constructor(){
		super();
		this.state={
			credentials:{username:"",
						password:""}
		};
		this.checkuser=this.checkuser.bind(this);
		this.setusername=this.setusername.bind(this);
		this.setpassword=this.setpassword.bind(this);
	}
	checkuser(){
		this.props.actions.checkuservalid(this.state.credentials);
	}
	setusername(event){
		console.log("iam inside username");
		const credentials = this.state.credentials;
		credentials.username = event.target.value;
		this.setState({credentials:credentials});

	}
	setpassword(event){
		console.log("iam inside password");
		const credentials = this.state.credentials;
		credentials.password = event.target.value;
		this.setState({credentials:credentials});
	}
	render(){
		return(
			<div className="col-lg-12">
				<h1>Royal Survay</h1>
				<h4>Login before you continue</h4>
				<div className="col-lg-12 loginform">
					<input type="text" placeholder="Username" className="form-control" onChange={this.setusername} value={this.state.credentials.username} />
					<br />
					<input type="password" placeholder="Password" className="form-control" onChange={this.setpassword} value={this.state.credentials.password} />
					<br />
					<button className="btn btn-md btn-primary login-button" onClick={this.checkuser}>Login</button>
				</div>

			</div>
			);
	}
}
function mapStateToProps(state , ownProps){
	return {
		credentials:state.credentials
	};
}
function mapDispatchToProps(dispatch){
	return{
		actions :bindActionCreators(loginActions , dispatch)
	};
}
export default connect(mapStateToProps , mapDispatchToProps)(HomePage);
