import React from 'react';
import { Route , IndexRoute} from 'react-router';
import App from './components/app.js';
import HomePage from './components/homepage/homepage';
import Landingpage from './components/landingpage/landingpage';

export default(
	<Route path="/" component={App}>
		<IndexRoute component={HomePage} />
		<Route path="/landingpage" component={Landingpage} />
	</Route>
);