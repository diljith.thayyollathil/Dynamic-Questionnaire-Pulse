import {combineReducers} from 'redux';
import logins from './loginReducer';
import questions from './questionreducer';

const rootReducer = combineReducers({
	logins,
	questions
});
export default rootReducer;