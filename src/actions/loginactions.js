import * as types from './actionTypes';
import { browserHistory } from 'react-router'

export function checkuservalidsuccess(states){
	console.log(states)
	return { type:types.CHECK_USER_VALID ,states :states};
}
export function checkuservalid(credentials){
	return function(dispatch) {
	    return fetch('/loginchecker', {
	      method: 'post',
	      body: JSON.stringify(credentials)
	    }).then(function(result) {
	        if (result.status === 200) {
	          return result.json();
	        }
	        throw "request failed";
	      })
	      .then(function(jsonResult) {
	        dispatch(checkuservalidsuccess(jsonResult));
	        browserHistory.push('/landingpage');
	      })
	      .catch(function(err) {
	        console.log("Oops...", "Couldn't fetch : ", err);
	      });
  };
}

