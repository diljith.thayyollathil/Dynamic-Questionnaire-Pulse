var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');

var loginSchema = mongoose.Schema({
  username: { type: String, required: false },
  password: { type: String, required: false },
},
{
  versionKey: false,
});

loginSchema.plugin(autoIncrement.plugin, { model:'login', startAt: 1, incrementBy: 1 });
module.exports = mongoose.model('login', loginSchema);
