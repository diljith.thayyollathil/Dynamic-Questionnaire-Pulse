var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');

var questionSchema = mongoose.Schema({
  question: { type: String, required: true },
  type: { type: String, required: true },
  answers: [{ type: String, required: false }],
  answered: { type: String, required: false },

},
{
  versionKey: false,
});

questionSchema.plugin(autoIncrement.plugin, { model:'questionproces', startAt: 1, incrementBy: 1 });
module.exports = mongoose.model('question', questionSchema);
