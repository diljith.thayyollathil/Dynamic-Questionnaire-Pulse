import express from 'express';
import webpack from 'webpack';
import path from 'path';
import config from '../webpack.config';
import open from 'open';
// database connection
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var connection = mongoose.connect('mongodb://localhost/redux');
autoIncrement.initialize(connection);

var Login =require('../models/loginmodel');
var Question = require('../models/processquestions');

/* eslint-disable no-console */

const port = 9900;
const app = express();
const compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('/', function(req, res) {
  res.sendFile(path.join( __dirname, '../src/index.html'));
});

// var questionarray = new Question();
// questionarray.question="What is your name";
// questionarray.type="select";
// questionarray.answers=["dirst","second","third","fourth"];
// questionarray.answered="";
// questionarray.save();



app.post("/loginchecker",function(req,res){
	var userslog=[{"username":"diljith" , "password":"asd"}, {"username":"anuj" , "password":"asd"}];
	if (req.method == 'POST') {
      req.on('data', function(data) {
        data = data.toString();
        data = JSON.parse(data);
       	for(var i=0;i<userslog.length ; i++){
       		console.log(userslog[i].username)
       		if(userslog[i].username==data.username && userslog[i].password==data.password){
       			res.json({username:data.username})
       		}
       	}
        
      });
  	}
});
app.get("/getallquestion",function(req,res){
  console.log("hello bro")
  Question.find({}, function(err, value) {
    if (err){
      console.log(err);
      res.end()
    }
    if (value){
      console.log("iam in server" , value);
      res.json(value)
    } else {
      res.json(null);
    }
  });
});

app.listen(port, function(err) {
  if (err) {
    console.log(err);
  } else {
    open(`http://localhost:${port}`);
  }
});